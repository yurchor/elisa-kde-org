# Elisa Website

This website use the hugo and require additionally the go binary.

## Run development

```
hugo serve
```

## Run production

```
hugo --minify
```

The configuration are located in `config.yml`.
