#!/usr/bin/env bash

FILENAME="elisa-kde-org_www"

function export_pot_file # First parameter will be the path of the pot file we have to create, includes $FILENAME
{
    potfile=$1
    i18n-hugo-extractor --directory './' extract $potfile
}

function import_po_files # First parameter will be a path that will contain several .po files with the format LANG.po
{
    export LANG=en_US.UTF-8
    podir=$1
    i18n-hugo-extractor --directory './' import $podir
    i18n-hugo-extractor --directory './' generate-translations
    rm -rf locale $podir
}
